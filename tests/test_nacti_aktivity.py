import unittest
import pandas as pd
from pandas.testing import assert_frame_equal
from fitbit.activity import nacti_aktivity


class TestNactiAktivity(unittest.TestCase):
    def setUp(self):
        self.expected_df = pd.DataFrame({})

    def test_nacti_aktivity(self):
        result = nacti_aktivity()
        assert_frame_equal(result, self.expected_df)


if __name__ == "__main__":
    unittest.main()
