import unittest
from fitbit.activity import max_rychlost


class TestMaxRychlost(unittest.TestCase):
    def test_max_rychlost(self):
        result = max_rychlost()
        expected_result = {
            "id": 123,
            "max_speed": 10.5,
        }
        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()
