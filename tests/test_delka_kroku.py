import unittest
from fitbit.activity import delka_kroku


class TestDelkaKroku(unittest.TestCase):
    def test_delka_kroku(self):
        result = delka_kroku()
        expected_result = "Průměrná délka kroku je: 0.5 m"
        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()
