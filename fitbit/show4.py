import matplotlib.pyplot as plt
from fitbit.activity import nacti_aktivity
from pathlib import Path


def plot_data():
    data_path = Path ("Data")
    subject = data_path / Path ("01/fitbit/activity_level.csv")
    df = nacti_aktivity(subject)
    fig = plt.figure(figsize=(15, 10))
    fig.suptitle("Test 4.4.2024")
    ax1 = fig.add_subplot(221)
    ax1.title.set_text("Graf: Počet kroků")
    plt.plot(df['steps'])
    plt.xlabel("Čas")
    plt.ylabel("Počet kroků")

    ax2 = fig.add_subplot(222)
    ax2.title.set_text("Graf: Vzdálenost")
    plt.plot(df['distance'])
    plt.xlabel("Čas")
    plt.ylabel("Vzdálenost")

    ax3 = fig.add_subplot(223)
    ax3.title.set_text("Graf: Rychlost")
    plt.plot(df['distance']*4)
    plt.xlabel("Čas")
    plt.ylabel("Rychlost")

    ax4 = fig.add_subplot(224)
    ax4.title.set_text("Graf: Délka kroku")
    plt.plot(df['distance']/df['steps']*1000)
    plt.xlabel("Čas")
    plt.ylabel("Délka kroku")

    plt.savefig("test.png")
    plt.show()
    
plot_data()
