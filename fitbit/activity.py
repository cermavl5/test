import pandas as pd


def nacti_aktivity(data_table_path_activity):
    df = pd.read_csv(data_table_path_activity)
    return df[df['steps'] > 0]  


def delka_kroku(data_table_path_activity):
    df = pd.read_csv(data_table_path_activity)
    df["step_length"] = (df["distance"] / df["steps"]) * 1000
    return "Průměrná délka kroku je: " + str(df["step_length"].mean()) + " m"


def max_rychlost(data_table_path_activity):
    df = pd.read_csv(data_table_path_activity)
    distance = df["distance"] 
    index = distance.idxmax()
    max = distance.max()
    speed = max * 4
    return index, speed
