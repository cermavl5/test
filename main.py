from fastapi import FastAPI
from pathlib import Path
import pandas as pd
from pydantic import BaseModel
from pandas_to_pydantic import dataframe_to_pydantic
from fitbit.activity import nacti_aktivity, delka_kroku, max_rychlost
from fitbit.show4 import plot_data
import numpy as np


class SubjectID(BaseModel):
    ID: int
    age: int
    gender: str
    max_speed: float
    avg_steps: float

class SubjectActivity(BaseModel):
    id: int
    name: str
    date: str
    calories: float
    steps: int
    distance: float
    floors: int
    elevation: float
    room: int
    timestamp: str

data_path = Path("Data")
data_table_path = data_path / "Profiles.xlsx"
data_table_path_activity = "Data/01/fitbit/activity_level.csv"


app = FastAPI()


@app.get("/")
def root():
    return {"message": "Test"}


@app.get("/aktivity/")
def aktivity():
    df = nacti_aktivity(data_table_path_activity)
    return dataframe_to_pydantic(df, SubjectActivity)   


@app.get("/delka_kroku/")
def delkaKroku():
    return delka_kroku(data_table_path_activity)


@app.get("/max_rychlost/")
def maxRychlost():
    return max_rychlost(data_table_path_activity)


@app.get("/subjects/")
def all_subjects():
    df_profiles = pd.read_excel(data_table_path)
    df = data_path.glob("**/*activity_level.csv")
    max_speeds = np.array([], dtype=float)
    avg_steps = np.array([], dtype=float)
    for f in df:
        df = pd.read_csv(f)
        max_speeds = np.append(max_speeds, df["distance"].max() * 4)
        avg_steps = np.append(avg_steps, df["steps"].mean())
    df_profiles["avg_steps"] = avg_steps
    df_profiles["max_speed"] = max_speeds
    return dataframe_to_pydantic(df_profiles, SubjectID)

@app.get("/subjects/{id}")
def subject(id: int):
    df = pd.read_excel(data_table_path)
    subject = df.loc[df["ID"] == id]
    df = pd.read_csv(data_path / Path("0" + str(id) + "/fitbit/activity_level.csv"))
    subject["avg_steps"] = df["steps"].mean()
    subject["max_speed"] = df["distance"].max() * 4
    return dataframe_to_pydantic(subject, SubjectID)

plot_data(data_path)